using System.Collections.Generic;

namespace Assessment
{
    public class StringProvider : IElementsProvider<string>
    {
        private readonly string separator = ",";
        private readonly string separator2 = "|";
        private readonly string separator3 = " ";

        public IEnumerable<string> ProcessData(string source)
        {
            return source.Split(separator);
        }

        public IEnumerable<string> ProcessData2(string source)
        {
            return source.Split(separator2);
        }
        public IEnumerable<string> ProcessData3(string source)
        {
            return source.Split(separator3);
        }
    }
}