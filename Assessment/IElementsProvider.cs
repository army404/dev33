using System.Collections.Generic;

namespace Assessment
{
    public interface IElementsProvider<T>
    {
        public IEnumerable<T> ProcessData(T source);
        public IEnumerable<T> ProcessData2(T source);
        public IEnumerable<T> ProcessData3(T source);
    }
}