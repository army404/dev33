using System.Collections.Generic;
using System.Linq;
using System;
namespace Assessment
{
    public class PaginationString : IPagination<string>
    {
        private readonly IEnumerable<string> data;
        private readonly int pageSize;
        private int currentPage;
        private string option;
        public PaginationString(string source, int pageSize, IElementsProvider<string> provider, string option)
        {
            switch (option)
            {
                case "1":
                    data = provider.ProcessData(source);
                      string str2 ="";
                     for(int i = 0; i < data.Count(); i++) 
                        {
                             str2 = str2+ data.ElementAt(i);
                        }
                         Console.WriteLine($"data : "+ str2);
                    break;
                case "2":
                     data = provider.ProcessData2(source);
                      string str1 ="";
                     for(int i = 0; i < data.Count(); i++) 
                        {
                             str1 = str1+ data.ElementAt(i);
                        }
                         Console.WriteLine($"data : "+ str1);
                    break;
                case "3":
                    data = provider.ProcessData3(source);
                      string str3 ="";
                     for(int i = 0; i < data.Count(); i++) 
                        {
                             str3 = str3+ data.ElementAt(i);
                        }
                         Console.WriteLine($"data : "+ str3);
                    break;
            }
           /* data = provider.ProcessData(source);
            */currentPage = 1; 
            this.pageSize = pageSize;
        }
        public void FirstPage()
        {
            currentPage = 1;
        }

        public void GoToPage(int page)
        {
            throw new System.NotImplementedException();
        }

        public void LastPage()
        {
            throw new System.NotImplementedException();
        }

        public void NextPage()
        {
            
        }

        public void PrevPage()
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<string> GetVisibleItems()
        {
            return data.Skip(currentPage*pageSize).Take(5);
        }

        public int CurrentPage()
        {
            throw new System.NotImplementedException();
        }

        public int Pages()
        {
            throw new System.NotImplementedException();
        }
    }
}